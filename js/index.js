$(function(){

	var myScroll;
	var flag = '';
	var h = document.getElementById('wrapper').clientHeight;//网页可见区域高
	function loaded () {
		myScroll = new IScroll('#wrapper', {
			 mouseWheel: true ,
			 probeType: 3,
			 deceleration: 0.006
		});

		myScroll.on("scroll",function(){
			if(this.y < this.maxScrollY-50){
				$(".more").show();
				flag = 'up';
			}else if(this.y > 60){
				$(".down").show();
				flag = 'down';
			}
			
		});

		myScroll.on("scrollEnd",function(){
			$(".more,.down").hide();
			if(flag == 'up'){
				addData();
				flag = false;
			}else if(flag == 'down'){
				//下拉刷新
				window.location.reload();
			}
			//回到顶部
			if (this.y < -h) {
				$("#top").show();
						//回到顶部
				$("#top").on("tap",function(){
					myScroll.scrollTo(0, 0, 600);
				});
			}else{
				$("#top").hide();
			}
		});
	}
	window.onload = loaded();
	document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);

		function addData(){
			var html = '';
			var l = $("li").length;
			$.ajax({
				type: 'get',
				url: 'ajax.php',
				datatype: 'json',
				success: function (data){
					var sdata = data.substr(0,data.indexOf('}')+1);
					var res = JSON.parse(sdata);
					if(res.status){
						for (var i = res.num; i >= 0; i--) {
							html += '<li>'
								+'<div class="left"><img src="img/monkey'+ (parseInt(Math.random()*4,10)+1) +'.jpg"></div>'
								+'<div class="right">'
								+'<div class="r-font">金猴迎春</div>'
								+'<div class="r-font">金猴报喜</div>'
								+'<div class="r-font">火树银猴</div>'
								+'</div>'
								+'</li>';
						}
						$(".ul").append(html);
						myScroll.refresh();
					}
				},
				error: function(){
					alert('error');
				}

			});
		}


})